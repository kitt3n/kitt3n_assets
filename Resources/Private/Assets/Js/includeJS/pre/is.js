var comparator = {
    '<': function(a, b) { return a < b; },
    '<=': function(a, b) { return a <= b; },
    '>': function(a, b) { return a > b; },
    '>=': function(a, b) { return a >= b; }
};
var compareVersion = function(version, range) {
    var string = (range + '');
    var n = +(string.match(/\d+/) || NaN);
    var op = string.match(/^[<>]=?|/)[0];
    return comparator[op] ? comparator[op](version, n) : (version == n || n !== n);
};
var platform = (navigator && navigator.platform || '').toLowerCase();
var userAgent = (navigator && navigator.userAgent || '').toLowerCase();
var vendor = (navigator && navigator.vendor || '').toLowerCase();

var isAndroid = function() {
    return /android/.test(userAgent);
};

var isAndroidPhone = function() {
    return /android/.test(userAgent) && /mobile/.test(userAgent);
};

var isAndroidTablet = function() {
    return /android/.test(userAgent) && !/mobile/.test(userAgent);
};

var isBlackberry = function() {
    return /blackberry/.test(userAgent) || /bb10/.test(userAgent);
};

var isChrome = function(range) {
    var match = /google inc/.test(vendor) ? userAgent.match(/(?:chrome|crios)\/(\d+)/) : null;
    return match !== null && !isOpera() && compareVersion(match[1], range);
};

var isDesktop = function() {
    return !isMobile() && !isTablet();
};

var isEdge = function(range) {
    var match = userAgent.match(/edge\/(\d+)/);
    return match !== null && compareVersion(match[1], range);
};

var isFirefox = function(range) {
    var match = userAgent.match(/(?:firefox|fxios)\/(\d+)/);
    return match !== null && compareVersion(match[1], range);
};

var isIe = function(range) {
    var match = userAgent.match(/(?:msie |trident.+?; rv:)(\d+)/);
    return match !== null && compareVersion(match[1], range);
};

var isInIframe = function() {
    return (window.location != window.parent.location) ? true : false;
};

var isIos = function() {
    return isIphone() || isIpad() || isIpod();
};

var isIpad = function(range) {
    var match = userAgent.match(/ipad.+?os (\d+)/);
    return match !== null && compareVersion(match[1], range);
};

var isIphone = function(range) {
    // avoid false positive for Facebook in-app browser on ipad;
    // original iphone doesn't have the OS portion of the UA
    var match = isIpad() ? null : userAgent.match(/iphone(?:.+?os (\d+))?/);
    return match !== null && compareVersion(match[1] || 1, range);
};

var isIpod = function(range) {
    var match = userAgent.match(/ipod.+?os (\d+)/);
    return match !== null && compareVersion(match[1], range);
};

var isLinux = function() {
    return /linux/.test(platform) && !isAndroid();
};

var isMac = function() {
    return /mac/.test(platform);
};

var isMobile = function() {
    return isIphone() || isIpod() || isAndroidPhone() || isBlackberry() || isWindowsPhone();
};

var isOffline = !isOnline;

var isOnline = function() {
    return !navigator || navigator.onLine === true;
};

var isOpera = function(range) {
    var match = userAgent.match(/(?:^opera.+?version|opr)\/(\d+)/);
    return match !== null && compareVersion(match[1], range);
};

var isOperaMini = function(range) {
    var match = userAgent.match(/opera mini\/(\d+)/);
    return match !== null && compareVersion(match[1], range);
};

var isPhantom = function(range) {
    var match = userAgent.match(/phantomjs\/(\d+)/);
    return match !== null && compareVersion(match[1], range);
};

var isSafari = function(range) {
    var match = userAgent.match(/version\/(\d+).+?safari/);
    return match !== null && compareVersion(match[1], range);
};

var isTablet = function() {
    return isIpad() || isAndroidTablet() || isWindowsTablet();
};

var isTouchDevice = function() {
    //return 'ontouchstart' in document.documentElement || window.Touch || 'createTouch' in document;
    //return ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
    return (("ontouchstart" in window) || window.navigator && window.navigator.msPointerEnabled && window.MSGesture || window.DocumentTouch && document instanceof DocumentTouch);
};

var isWindows = function() {
    return /win/.test(platform);
};

var isWindowsPhone = function() {
    return isWindows() && /phone/.test(userAgent);
};

var isWindowsTablet = function() {
    return isWindows() && !isWindowsPhone() && /touch/.test(userAgent);
};


var isInViewport = function(el) {
    var rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
};

var isOnScreen = function(el) {
    var rect = el.getBoundingClientRect();

    var windowHeight = (window.innerHeight || document.documentElement.clientHeight);
    var windowWidth = (window.innerWidth || document.documentElement.clientWidth);

    var vertInView = (rect.top <= windowHeight) && ((rect.top + rect.height) >= 0);
    var horInView = (rect.left <= windowWidth) && ((rect.left + rect.width) >= 0);

    return (vertInView && horInView);
};