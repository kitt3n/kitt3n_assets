##################
## CONFIG SETUP ##
##################
config {
    ## CSS compression
    compressCss = 1
    concatenateCss = 1

    ## JS compression
    compressJs = 1
    concatenateJs = 1
}

[applicationContext == "Development"]
    config {
        compressCss = 0
        concatenateCss = 0
        compressJs = 0
        concatenateJs = 0
    }
[END]