plugin {
    tx_kitt3nassets {
        settings {
            includeJSFooterlibs {
                aos = 0
                hyphenator = 0
            }

            head {
                prefetch {
                    google {
                        fonts = 0
                        ajax = 0
                    }
                    cloudflare {
                        cdnjs = 0
                    }
                }
                msapplication {
                    # "#000000" equals empty value
                    tile_color = #000000
                }
                theme_color = #000000
            }
        }
    }
}